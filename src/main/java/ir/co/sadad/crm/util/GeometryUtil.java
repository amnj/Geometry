package ir.co.sadad.crm.util;

import ir.co.sadad.crm.model.Point;

/**
 * Operations related to geometry
 * */
public class GeometryUtil {

    /**
     * calculate distance between source and destination
     * @param source
     * @param destination
     * @return double (distance)
     * */
    public static double calculateDistanceBetweenTwoPoints(Point source, Point destination){
        double earthRadius = 6371;
        double longDistance = Math.toRadians(destination.getLongitude()-source.getLongitude());
        double latDistance = Math.toRadians(destination.getLatitude()-source.getLatitude());
        double sinLogDistance = Math.sin(longDistance/2);
        double sinLatDistance = Math.sin(latDistance/2);
        double underSqrtResult =  Math.pow(sinLatDistance,2)+(Math.pow(sinLogDistance,2)
        *Math.cos(Math.toRadians(destination.getLatitude()))*Math.cos(Math.toRadians(source.getLatitude())));
        double c = 2 * Math.atan2(Math.sqrt(underSqrtResult),Math.sqrt(1-underSqrtResult));
        double distance =  c * earthRadius;
        return distance;

    }
}
