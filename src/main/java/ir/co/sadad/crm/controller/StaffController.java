package ir.co.sadad.crm.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import ir.co.sadad.crm.domain.People;
import ir.co.sadad.crm.domain.Staff;
import ir.co.sadad.crm.model.Point;
import ir.co.sadad.crm.util.GeometryUtil;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/***
 * Operations related to staff
 */
public class StaffController {

    public Staff getStaff() throws Exception {

        try {
            return new ObjectMapper().readValue(StaffController.class.getClassLoader().getResource("staff.json"), Staff.class);
        } catch (IOException e) {
            Logger.getLogger(StaffController.class.getName()).severe(e.getMessage());
            throw e;
        }
    }

    /**
     * Retrieve people with particular distance
     */
    public List<People> getPeopleByDistance(Double distance, Staff staff, Point sadadPoint) {
        return staff.getPeople().stream().filter((people) -> {
                    double v = GeometryUtil.calculateDistanceBetweenTwoPoints(sadadPoint
                            , new Point(people.getLatitude(), people.getLongitude()));
                    return distance > v;
                }
        ).collect(Collectors.toList());

    }

}
