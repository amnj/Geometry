package ir.co.sadad.crm.util;

import ir.co.sadad.crm.controller.StaffController;
import ir.co.sadad.crm.model.Point;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import static org.junit.jupiter.api.Assertions.assertTrue;


@RunWith(JUnitPlatform.class)
class GeometryUtilTest {

    StaffController staffController = null;
    Point source;
    double testDistance;

    @BeforeEach
    void setUp() {
        testDistance = 50d;
        source = new Point(35.745108,51.451365);
    }

    @Test
    void calculateDistanceBetweenTwoPointsGreaterThanGivenDistance() {
        Point destination = new Point(34.6885018, 50.7116357);
        assertTrue(testDistance < GeometryUtil.calculateDistanceBetweenTwoPoints(source, destination));
    }


    @Test
    void calculateDistanceBetweenTwoPointsLessThanGivenDistance() {
        Point destination = new Point(35.817676, 51.516311);
        assertTrue(testDistance > GeometryUtil.calculateDistanceBetweenTwoPoints(source, destination));
    }
}