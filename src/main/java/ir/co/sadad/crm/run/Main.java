package ir.co.sadad.crm.run;

import ir.co.sadad.crm.controller.StaffController;
import ir.co.sadad.crm.domain.People;
import ir.co.sadad.crm.domain.Staff;
import ir.co.sadad.crm.model.Point;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

public class Main {

    public static void main(String[] args) {
        Properties prop = new Properties();
        final Point sadadPoint = new Point();
        try (InputStream inputStream =
                     new FileInputStream(Main.class.getClassLoader().getResource("sadad-location.properties").getFile())) {
            prop.load(inputStream);
            sadadPoint.setLatitude(Double.valueOf(prop.getProperty("sadad-latitude")));
            sadadPoint.setLongitude(Double.valueOf(prop.getProperty("sadad-longitude")));
        } catch (Exception e) {
            e.printStackTrace();
        }

        StaffController peopleController = new StaffController();
        Staff staff = null;
        try {
            staff = peopleController.getStaff();
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<People> peopleByDistance = peopleController.getPeopleByDistance(50d, staff, sadadPoint);
        System.out.println("people should be invited : \n");
        for (People people : peopleByDistance) {
            System.out.println( people.getName());
        }

    }

}
