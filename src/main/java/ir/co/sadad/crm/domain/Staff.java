package ir.co.sadad.crm.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;


public class Staff {

    @JsonProperty(value="people")
    private List<People> people;

    public List<People> getPeople() {
        return people;
    }

    public void setPeople(List<People> people) {
        this.people = people;
    }


}


